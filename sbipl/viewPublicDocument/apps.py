from django.apps import AppConfig


class ViewpublicdocumentConfig(AppConfig):
    name = 'viewPublicDocument'
