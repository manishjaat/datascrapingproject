from django import forms

class cin_(forms.Form):
	cin=forms.CharField(label='Company CIN/FCRN',required=True,strip=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Company CIN/FCRN'}))
	
class llp_(forms.Form):
	llp=forms.CharField(label='Company LLPIN/FLLPIN',required=True,strip=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Company LLPIN/FLLPIN'}))
	