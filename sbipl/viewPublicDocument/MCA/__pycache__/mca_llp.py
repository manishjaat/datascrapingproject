import requests
import bs4
def viewDocuments():
	url='http://mca.gov.in/mcafoportal/viewDocuments.do'
	#url='http://mca.gov.in/mcafoportal/vpdCheckCompanyStatus.do'
	payload='companyOrllp=L&cartType=&__checkbox_companyChk=true&__checkbox_cinChk=true&__checkbox_llpChk=true&llpinChk=true&__checkbox_llpinChk=true&llpFDetails=AAA-6349&__checkbox_regStrationNumChk=true&countryOrigin=INDIA&__checkbox_stateChk=true&displayCaptcha=&companyID=AAA-6349'
	headers = {
		  'cache-control': 'max-age=0',
		  'upgrade-insecure-requests': '1',
		  'origin': 'https://my.gstzen.in',
		  'content-type': 'application/x-www-form-urlencoded',
		  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36',
		  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
		  'sec-fetch-site': 'same-origin',
		  'sec-fetch-mode': 'navigate',
		  'sec-fetch-user': '?1',
		  'sec-fetch-dest': 'document',
		  'referer': 'http://mca.gov.in/mcafoportal/viewDocuments.do',
		  'accept-language': 'en-US,en;q=0.9',
		  'cookie': 'HttpOnly; JSESSIONID=0000QbCd8DjxZm8ZYQSacmOMMuX:1bp6oqb3d'
		}

	response = requests.request("POST", url, headers=headers, data = payload)
	print(response)
	#print(response.text)
	with open("mca_llp.html","w") as f:
		f.write(response.text)

if __name__=="__main__":

	viewDocuments()