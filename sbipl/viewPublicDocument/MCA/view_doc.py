import requests
import bs4
import pandas as pd
#doc_cat_=['CETF','CDRD','INCD','CRGD','ARBE','LLPF','OTRE','OTRA']
cat_={'CETF':'Certificates','CDRD':'Change in Directors','INCD':'Incorporation Documents','CRGD':'Charge Documents','ARBE':'Annual Returns and Balance Sheet eForms','LLPF':'LLP Forms(Conversion of company to LLP)','OTRE':'Other eForm Documents','OTRA':'Other Attachments'}
#doc_year_=['2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
def viewDocuments(companyName,Years=None):
	if Years!=None:
		doc_year_=Years
	else:
		import datetime
		curr_date=datetime.date.today()
		curr_year=curr_date.year
		st_year=2006
		doc_year_=[str(2006+st_year) for st_year in range(0,curr_year-st_year+1)]
	
	results={}
	categoryName='ARBE'
	finacialYear='2018'
	cat_wise_data={}
	for categoryName in cat_:
		#print(categoryName)

		year_wise_data={}
		for finacialYear in doc_year_:
			#url='http://mca.gov.in/mcafoportal/viewDocuments.do'
			url='http://mca.gov.in/mcafoportal/vpdDocumentCategoryDetails.do'
			payload='cinFDetails=AAA-6349&companyName='+companyName+'&cartType=&categoryName='+categoryName+'&finacialYear='+finacialYear#2018'
			headers = {
				  'cache-control': 'max-age=0',
				  'upgrade-insecure-requests': '1',
				  'origin': 'https://my.gstzen.in',
				  'content-type': 'application/x-www-form-urlencoded',
				  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36',
				  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
				  'sec-fetch-site': 'same-origin',
				  'sec-fetch-mode': 'navigate',
				  'sec-fetch-user': '?1',
				  'sec-fetch-dest': 'document',
				  'referer': 'http://mca.gov.in/mcafoportal/vpdDocumentCategoryDetails.do',
				  'accept-language': 'en-US,en;q=0.9',
				  'cookie': 'HttpOnly; JSESSIONID=0000QbCd8DjxZm8ZYQSacmOMMuX:1bp6oqb3d'
				}

			response = requests.request("POST", url, headers=headers, data = payload)
			#print(response)
			response_text=response.text
			'''
			with open("view_doc.html","w") as f:
				f.write(response_text)
			'''
			
			soup = bs4.BeautifulSoup(response_text,'lxml')
			try:
				table = soup.find('table',class_='result-forms_vpd')
				df = pd.read_html(str(table))
				row_data=df[0].to_dict(orient='records')
				#print(finacialYear)
				#print(row_data)
				year_wise_data[finacialYear]=row_data
				#print(year_wise_data)

			except:
				pass
		#print(year_wise_data)
		if year_wise_data!={}:
			cat_wise_data[cat_[categoryName]]=year_wise_data
		else:
			pass
	return cat_wise_data

