import requests
import bs4
import pandas as pd


def DIN_(din_number):
	url = "http://www.mca.gov.in/mcafoportal/enquireDIN.do"#"http://www.mca.gov.in/mcafoportal/showEnquireDIN.do"
	payload = 'DIN='+din_number+'&displayCaptcha='
	headers = {
	  'authority': 'http://www.mca.gov.in',
	  'cache-control': 'max-age=0',
	  'upgrade-insecure-requests': '1',
	  'origin': 'http://www.mca.gov.in',
	  'content-type': 'application/x-www-form-urlencoded',
	  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36',
	  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
	  'sec-fetch-site': 'same-origin',
	  'sec-fetch-mode': 'navigate',
	  'sec-fetch-user': '?1',
	  'sec-fetch-dest': 'document',
	  'referer': 'http://www.mca.gov.in/mcafoportal/enquireDIN.do',
	  'accept-language': 'en-US,en;q=0.9',
	  'cookie': 'HttpOnly; JSESSIONID=0000GajLVpI592mNoDiFvlraI3j:1af04ar01'
	}
	
	try:
		response = requests.request("POST", url, headers=headers, data = payload)
	except:
		return "Request Error : "+url 
	
	
	'''print(response.text.encode('utf8'))'''
	#print(response.text)
	response_text=response.text
	'''
	print(response.url)
	with open('din.html','w') as html:
		html.write(response_text)
	'''


	soup=bs4.BeautifulSoup(response_text,'lxml')
	
	try:
		din_tab=soup.find('table',class_='result-forms')
		df = pd.read_html(str(din_tab))
		row_data=df[0].to_dict(orient='records')
	except:
		return "DIN Status Error"
	#print(row_data)
	return row_data

if __name__ == '__main__':
	DIN_("00000001")