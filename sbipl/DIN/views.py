from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import din_form_
from .din import DIN_

# Create your views here.
def din_form(request):
	if request.method=='GET':
		form=din_form_(request.GET)
		if form.is_valid():
			din=form.cleaned_data['din']
			if din!='':
				din_detail=DIN_(din)
				return JsonResponse(din_detail, safe=False)


	form=din_form_()
	return render(request,'index.html',{'form':form})
