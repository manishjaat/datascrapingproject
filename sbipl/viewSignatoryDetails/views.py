from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import viewSignatoryDetails_
from .main import signatory


# Create your views here.
def viewSignatoryDetails_form(request):
	
	if request.method=='GET':
		form=viewSignatoryDetails_(request.GET)
		if form.is_valid():
			FDetails=form.cleaned_data['viewSignatory']
			print(FDetails)
			
			if FDetails!='':
				viewDoc=signatory.signatoryDetails(FDetails)
				
				return JsonResponse(viewDoc, safe=False)
			
			


	form=viewSignatoryDetails_()
	#return HttpResponse('GSTIN Deatil Form')
	return render(request,'viewSignatoryDetails.html',{'form':form})
