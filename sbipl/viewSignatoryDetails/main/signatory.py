import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd
from azcaptchaapi import AZCaptchaApi
#webdriver.Chrome(PATH) or webdriver.Firefox()
#cin="L17110MH1973PLC019786"
def signatoryDetails(llp):
	def captcha_find(driver):
		captcha=driver.find_element_by_id("captcha")
		captcha_png = captcha.screenshot_as_png

		with open("captcha.png", "wb") as png:
			png.write(captcha_png)

		#cap=input("Enter Captcha:")
		cap=""
		api = AZCaptchaApi('knb8v62q7zcptzhqghjrgmdrxkvlnwyb')
		try:
			with open("captcha.png", 'rb') as captcha_file:
				captcha = api.solve(captcha_file)
				cap=captcha.await_result()

		except:
			cap=input("Enter Captcha: ")
		
		return cap

	driver=webdriver.Chrome("chromedriver")
	#driver.set_window_position(-10000,0)
	driver.get('http://www.mca.gov.in/mcafoportal/viewSignatoryDetails.do')
	#driver.quit()

	companyID=driver.find_element_by_name("companyID")
	companyID.clear()
	companyID.send_keys(llp)

	cap=""
	accept=True
	while accept:
		try:
			cap=captcha_find(driver)

			userEnteredCaptcha=driver.find_element_by_name("userEnteredCaptcha")
			userEnteredCaptcha.send_keys(cap)

			submitBtn=driver.find_element_by_name("submitBtn")
			submitBtn.send_keys(Keys.RETURN)

			signatoryDetails=driver.find_element_by_id("signatoryDetails")
			accept=False
		except:
			msgboxclose=driver.find_element_by_id("msgboxclose")
			msgboxclose.click()
			cap=captcha_find(driver)

			userEnteredCaptcha=driver.find_element_by_name("userEnteredCaptcha")
			userEnteredCaptcha.send_keys(cap)

			submitBtn=driver.find_element_by_name("submitBtn")
			submitBtn.send_keys(Keys.RETURN)
			try:
				signatoryDetails=driver.find_element_by_id("signatoryDetails")
				accept=False
			except:
				accept=True




	#<table class="result-forms" align="center" id="signatoryDetails">
	str_tab="<table>"+str(signatoryDetails.get_attribute('innerHTML'))+"</table>"
	#print(str_tab)
	df = pd.read_html(str_tab)
	row_data=df[0].to_dict(orient='records')
	driver.quit()
	#print(row_data)
	return row_data

