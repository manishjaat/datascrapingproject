import requests
import bs4
import json
import re
from .get_latest import cap

def taxpayerData(gstin,pan,name):
	url = "https://my.gstzen.in/m/taxpayer-list/?"
	payload = 'csrfmiddlewaretoken=iZ8i8ueDBZbSD4zEfYjdOKMGlVfZIKYP2Cnlojfhjtv60ko83MEnMaydSKNmUmLD&csrfmiddlewaretoken=iZ8i8ueDBZbSD4zEfYjdOKMGlVfZIKYP2Cnlojfhjtv60ko83MEnMaydSKNmUmLD&gstin='+gstin+'&pan='+pan+'&name='+name
	headers = {
	  'authority': 'my.gstzen.in',
	  'cache-control': 'max-age=0',
	  'upgrade-insecure-requests': '1',
	  'origin': 'https://my.gstzen.in',
	  'content-type': 'application/x-www-form-urlencoded',
	  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36',
	  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,/;q=0.8,application/signed-exchange;v=b3;q=0.9',
	  'sec-fetch-site': 'same-origin',
	  'sec-fetch-mode': 'navigate',
	  'sec-fetch-user': '?1',
	  'sec-fetch-dest': 'document',
	  'referer': 'https://my.gstzen.in/m/taxpayer-list/',
	  'accept-language': 'en-US,en;q=0.9',
	  'cookie': 'csrftoken=SezA2B1hBW7229ar0bv7y19nHTAe2pmDCRODiq2VjqrgppZVOZQhwrVUeI8Be19r; _ga=GA1.2.1304182696.1598109533; _gid=GA1.2.107486105.1598109533; _gat=1; crisp-client%2Fsession%2F0784bb05-12e5-4f9f-b5fb-2e828026517c=session_a632d295-8631-49e8-9a7d-4369c13cedfc; csrftoken=SezA2B1hBW7229ar0bv7y19nHTAe2pmDCRODiq2VjqrgppZVOZQhwrVUeI8Be19r'
	}
	
	try:
		response = requests.request("POST", url, headers=headers, data = payload)
	except:
		return "Request Error : "+url 
	
	
	'''print(response.text.encode('utf8'))'''
	#print(response.text)
	response_text=response.text

	
	if (gstin!='') & (pan=='') & (name==''):
		return extsoup(response_text,gstin)
	else:
		return tplist(response_text)

def tplist(response_text):
	soup=bs4.BeautifulSoup(response_text,'lxml')
	taxpayer_details_list={}
	i=1
	c=0

	taxpayer_details_list[i]=[]
	try:
		taxpayer_details=soup.find('div',class_='table-responsive').find_all('td',class_='text-left')
	except:
		return "class error: table-responsive"
	
	for td in taxpayer_details:
		#print(td.text.strip())
	
		if c<5:
			taxpayer_details_list[i].append(td.text.strip())
			c+=1
		else:
			c=0
			i+=1
			taxpayer_details_list[i]=[]
			taxpayer_details_list[i].append(td.text.strip())
			c+=1
	#print(i,":",taxpayer_details_list)
	#JSON
	'''
	taxpayer.json:
	"Tax Payers": {"row number": ["Trade Name", "GSTIN", "PAN", "State", "Pin Code"],
	'''
	taxpayer_json={
	"Tax Payers":taxpayer_details_list
	}

	try:
		return taxpayer_json
	except:
		return "Json Error"
	
	

def extsoup(response_text,gstin):
	soup=bs4.BeautifulSoup(response_text,'lxml')
	
	#First Data
	try:
		desc=soup.find('div',class_='my-3').find('div',class_='card-block')
		desc="".join(" ".join(desc.text.split("\t")).split("\n"))
	except:
		return "First Table Error"
	
	

	#GSTIN Details
	gstin_details={}
	try:
		left_=soup.find('div',class_='col-sm-7').find_all('td',class_='text-muted')
		#right_=soup.find('div',class_='col-sm-7').find_all('td', attrs = {"class":"text-left"})
		right_=soup.select("td[class=text-left]")
	except:
		return "GSTIN Details Error"
	
	
	
	for key,value in zip(left_,right_):
		key=re.sub(' +',' ',"".join(" ".join(key.text.split("\t")).split("\n")).strip())
		value=re.sub(' +',' ',"".join(" ".join(value.text.split("\t")).split("\n")).strip())
		gstin_details[key]=value

	#Updated Details
	try:
		captcha_=soup.find('a',class_='btn btn-success btn-sm')
	except:
		return "Captcha Button Error"
	
	if captcha_!=None:
		tax_returns=cap(gstin)
	

	#JSON
	'''
	taxpayerlist.json:
	desc:"//Description//",
	gstin_d :  {
				"Legal Name": "__",
				"Company Status": "__", 
				"Trade Name": "__",
				"GSTIN": "__",
				"PAN": "__",
				"State": "__",
				"PIN Code": "__", 
				"TaxPayer Type": "__",
				"Constitution of Business": "__",
				"Registration Date": "__", 
				"Cancellation Date": "__", 
				"State Jurisdiction": "__",
				"Centre Jurisdiction": "__", 
				"Nature of Business Activities": "__"
				}
	 "tax_returns": {"row number": ["Return Type", "Return Period", "Date of Filing", "Status"],}

	'''
	taxpayer_list={
	"desc":re.sub(' +',' ',desc),
	"gstin_d":gstin_details,
	"tax_returns":tax_returns
	}
	#print(len(taxpayer_list))

	return taxpayer_list

	

if __name__ == '__main__':
	pan=''
	name=''
	gstin=''
	gstin=input("GSTIN Number: ").strip()
	pan=input("PAN Number: ").strip()
	name=input("Tax Payer Name: ").strip()
	taxpayerData(gstin,pan,name)
