from django import forms

class gstin_form(forms.Form):
	gstin=forms.CharField(label='To search by a GSTIN number, enter it here.',required=False,strip=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'GSTIN Number'}))
	pan=forms.CharField(label='To search by a PAN number, enter it here.',required=False,strip=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'PAN Number'}))
	name=forms.CharField(label='To search a Tax Payer using their name, enter it here.',required=False,strip=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Tax Payer Name'}))

	