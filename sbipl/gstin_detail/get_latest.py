from robobrowser import RoboBrowser
from azcaptchaapi import AZCaptchaApi
import base64

def solve(file):
    try:
        api = AZCaptchaApi('dt3gng2qckf9vwqp8wzcmrnjjybxhz6r')
        with open(file, 'rb') as captcha_file:
            captcha = api.solve(captcha_file)
            return captcha.await_result()
    except:
        return input("Captcha :")
        
def cap(gstin):
    taxpayer_returns_list={}
    url="https://my.gstzen.in/p/search-taxpayer/captcha/?cz%3Ainitial%3Agstin="+gstin+"&cz%3Ainitial%3Anext=None"
    # create a RoboBrowser object
    browser = RoboBrowser(history = True,parser='html.parser')

    
    browser.session.headers = {
	  'authority': 'my.gstzen.in',
	  'cache-control': 'max-age=0',
	  'upgrade-insecure-requests': '1',
	  'origin': 'https://my.gstzen.in',
	  'content-type': 'application/x-www-form-urlencoded',
	  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36',
	  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
	  'sec-fetch-site': 'same-origin',
	  'sec-fetch-mode': 'navigate',
	  'sec-fetch-user': '?1',
	  'sec-fetch-dest': 'document',
	  'referer': 'https://my.gstzen.in/p/search-taxpayer/captcha/?cz%3Ainitial%3Agstin='+gstin+'&cz%3Ainitial%3Anext=None',
	  'accept-language': 'en-US,en;q=0.9',
	  'cookie': 'csrftoken=gJI3q2l1d2BFc9fjUHOdDTKzBJhlXE7BG9Uy1xogmDvkyX8wB48ZanaEw4S75yJf; _ga=GA1.2.534844565.1598088957; fpestid=C2OxXWUTdhZ-1aqXrnan36d5sL-KzsPueDciiQj-S0YjD4erIKwpzbt50Wd-owIi00rI_w; _gid=GA1.2.639264123.1598442648; crisp-client%2Fsession%2F0784bb05-12e5-4f9f-b5fb-2e828026517c=session_831427f9-b8b4-4c77-b9fd-28834492738b; _gat=1'
	}
	
    browser.open(url)
    cap_form = browser.get_form()

    #image
    img_tag=browser.find('div',class_='card-block text-center').img['src']
    encoded_img=img_tag.split(',')[1]
    imgdata = base64.b64decode(encoded_img)
    filename = 'captcha.png'
    with open(filename, 'wb') as f:
        f.write(imgdata)
    solved_captcha=solve(filename)
    print(solved_captcha)
    accept=True
    while accept:
        try:
            cap_form['captcha'].value = solved_captcha
            browser.submit_form(cap_form)
            tab=browser.find('div',class_='table-responsive').find_all('td',class_='text-center')
            #src=str(browser.parsed())
            #print(src)
            i=1
            c=0
        
            taxpayer_returns_list[i]=[]
        
            for td in tab:
                #print(td.text.strip())
            
                if c<4:
                    taxpayer_returns_list[i].append(td.text.strip())
                    c+=1
                else:
                    c=0
                    i+=1
                    taxpayer_returns_list[i]=[]
                    taxpayer_returns_list[i].append(td.text.strip())
                    c+=1
            return taxpayer_returns_list

            accept=False

        except:
            cap(gstin)
            accept=False
            '''
            browser.open(url)
            cap_form = browser.get_form()
            #image
            img_tag=browser.find('div',class_='card-block text-center').img['src']
            encoded_img=img_tag.split(',')[1]
            imgdata = base64.b64decode(encoded_img)
            filename = 'captcha.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)
            solved_captcha=solve(filename)
            print(solved_captcha)
    
            cap_form['captcha'].value = solved_captcha
            browser.submit_form(cap_form)
            tab=browser.find('div',class_='table-responsive').find_all('td',class_='text-center')

            #src=str(browser.parsed())
            #print(src)
            i=1
            c=0
        
            taxpayer_returns_list[i]=[]
        
            for td in tab:
                #print(td.text.strip())
            
                if c<4:
                    taxpayer_returns_list[i].append(td.text.strip())
                    c+=1
                else:
                    c=0
                    i+=1
                    taxpayer_returns_list[i]=[]
                    taxpayer_returns_list[i].append(td.text.strip())
                    c+=1
            return taxpayer_returns_list

            accept=False
            '''






